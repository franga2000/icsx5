
ICSx⁵
========

Please see the [ICSx⁵ Web site](https://icsx5.bitfire.at) for comprehensive information about ICSx⁵.

ICSx⁵ is licensed under the [GPLv3 License](LICENSE).

News and updates: [@icsx5app](https://twitter.com/icsx5app)

Help and discussion: [ICSx⁵ forum](https://icsx5.bitfire.at/forums/)

ICSx⁵ is a Android 4.0+ app to subscribe to remote or local iCalendar files (like
time tables of your school/university or event files of your sports team).


License 
=======

Copyright (C) [bitfire web engineering](https://www.bitfire.at) (Stockmann, Hirner GesnbR).

This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome
to redistribute it under the conditions of the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).

ICSx⁵ uses these libraries (more information available in the app):

* [Ambilwarna Color Picker](https://github.com/yukuku/ambilwarna) (Apache License 2.0)
* [Android Support Library](https://developer.android.com/tools/support-library/) (Apache License 2.0)
* [Apache Commons](https://commons.apache.org) (Apache License 2.0)
* [ical4android](https://gitlab.com/bitfireAT/ical4android) (GPLv3)
  * [bnd, OSGI Core](http://bnd.bndtools.org) (Apache License 2.0)
  * [ical4j](https://github.com/ical4j/ical4j) (BSD 3-Clause License)
  * [slf4j](http://www.slf4j.org) (MIT License)
* [MemorizingTrustManager](https://github.com/ge0rg/MemorizingTrustManager) (MIT License)
